/**
 *
 * @author Scott Taj
 */
import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.*;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.TimeZone;
import javax.swing.*;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel; 
import org.jfree.chart.JFreeChart; 
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset; 
import org.jfree.data.category.DefaultCategoryDataset; 

//Dynamic color change stuff.

class DifferenceBarRenderer extends BarRenderer {
  public DifferenceBarRenderer() {
    super();
  }
  @Override
  public Paint getItemPaint(int x_row, int x_col) {
    CategoryDataset l_jfcDataset = getPlot().getDataset();
    String l_rowKey = (String)l_jfcDataset.getRowKey(x_row);
    String l_colKey = (String)l_jfcDataset.getColumnKey(x_col);
    double l_value  = l_jfcDataset.getValue(l_rowKey, l_colKey).doubleValue();
    if (l_value > 15.00 && l_value < 25.00) { return Color.yellow; }
    else if (l_value > 25.00) { return Color.red; }
    else { return Color.green; }
  }
}

public class MonitOR 
{
    
    public static double totalTimeOpened = 0;
    public static double timeIntoCase = 0;
    public static double ratio = 0;
    public static double caseStartTime;
    public static int doorCount = 0;
    public static String formattedCaseTime="";
    public static DefaultCategoryDataset myDataset1 = new DefaultCategoryDataset();
    public static DefaultCategoryDataset myDataset2 = new DefaultCategoryDataset();
    public static DateFormat timeFormat = new SimpleDateFormat ("HH:mm:ss"); 
    
    public static JLabel timeLabel = new JLabel();
    public static JLabel ratioLabel = new JLabel ();
    public static JLabel doorCountLabel = new JLabel ();
    //public static JLabel totalTimeLabel = new JLabel ();
        
    
    
    private static class WriteData implements Runnable 
    {
        private String myData;
        public WriteData (String data)
        {
            myData = data;
        }
        public void run() 
        {
            System.out.println ("Thread started.");
            InetAddress ip = null;
            try
            {
                ip = InetAddress.getByName("172.20.251.74");
                
            }
            catch (UnknownHostException e)
            {
                System.err.println("Unknown Host Exception.");
            }
            
            int portNumber = 44300;
            Socket mySocket = null;
            BufferedOutputStream toServer = null;
            OutputStreamWriter writeToServer = null;
            
            
        
            
            try
            {
                mySocket = new Socket (ip, portNumber);
                toServer = new BufferedOutputStream(mySocket.getOutputStream());
                writeToServer = new OutputStreamWriter (toServer);
                mySocket.setSoTimeout (10000);
                try
                {
                    writeToServer.write (myData);
                    writeToServer.flush ();
                    writeToServer.close();
                    
                    
                    
                }
                catch (IOException e)
                {

                    System.out.println("Unable to write data to server. Writing data to local file.");
                    FileWriter rewriter = new FileWriter ("LocalData.txt", true);
                    BufferedWriter outputRewriter = new BufferedWriter (rewriter);
                    outputRewriter.write(myData);
                    outputRewriter.newLine();
                    outputRewriter.flush();
                    

                }
                mySocket.close();
            }
            catch (IOException e)
            {
                System.out.println ("Unable to connect to server. Writing data to local file.");
                
                 try
                 {
                    FileWriter rewriter = new FileWriter ("LocalData.txt", true);
                    BufferedWriter outputRewriter = new BufferedWriter (rewriter);
                    outputRewriter.write(myData);
                    outputRewriter.newLine();
                    outputRewriter.flush();
                 }
                 catch (IOException ioe)
                 {
                     System.err.println ("Unable to write back to local file");
                     
                 }

            }
            
            
        }
    }
    
    public JPanel gui ()
    {
        //JFreeChart barChart1 = ChartFactory.createBarChart("", "", "", myDataset1);
        JFreeChart barChart2 = ChartFactory.createBarChart("", "", "", myDataset2);
        //ChartPanel barPanel1 = new ChartPanel (barChart1);
        ChartPanel barPanel2 = new ChartPanel (barChart2);
        //barPanel1.setPreferredSize (new java.awt.Dimension(160,190));
        barPanel2.setPreferredSize (new java.awt.Dimension(160,190));
        
        //CategoryPlot plot1 = barChart1.getCategoryPlot();
        CategoryPlot plot2 = barChart2.getCategoryPlot();
        NumberAxis range = (NumberAxis) plot2.getRangeAxis();
        range.setRange(0, 50);
        //barPanel1.getChart().removeLegend();
        barPanel2.getChart().removeLegend();
        
        //final BarRenderer renderer1 = (BarRenderer) plot1.getRenderer();
        //renderer1.setSeriesPaint(0, Color.green);
//        final BarRenderer renderer2 = (BarRenderer) plot2.getRenderer();
        plot2.setRenderer(new DifferenceBarRenderer());
        
        JPanel myPanel = new JPanel(new BorderLayout());
        myPanel.setPreferredSize(new java.awt.Dimension(320,240));
        //myPanel.add(barPanel1, BorderLayout.LINE_START);
        myPanel.add(barPanel2, BorderLayout.LINE_END);

	JPanel infoPanel = new JPanel(new GridLayout(1,1));
	infoPanel.setPreferredSize(new java.awt.Dimension(320,50));
	myPanel.add(infoPanel, BorderLayout.PAGE_END);
        
        JPanel infoPanel2 = new JPanel (new GridLayout(2,1));
        infoPanel2.setPreferredSize(new java.awt.Dimension(160,190));
        myPanel.add(infoPanel2, BorderLayout.LINE_START);
        
        timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date time = new Date((long)(timeIntoCase*1000));
        formattedCaseTime = timeFormat.format(time);
        timeLabel = new JLabel ("<html>Case Time:<br>" + formattedCaseTime + "</html>");
        ƒLabel = new JLabel ("<html>% Opened:" + String.format("<br>%.2f", ratio)  + "%</html>");
        doorCountLabel = new JLabel("Opened " + doorCount + " times for "+ String.format ("%.1f",totalTimeOpened) + " sec.");
        //totalTimeLabel = new JLabel ("Total:" + String.format ("%.1f",totalTimeOpened));
        timeLabel.setFont(new Font("Serif", Font.PLAIN, 24));
        ratioLabel.setFont(new Font("Serif", Font.PLAIN, 24));
        doorCountLabel.setFont(new Font("Serif", Font.PLAIN, 18));
        //totalTimeLabel.setFont(new Font("Serif", Font.PLAIN, 12));
//        timeLabel.setBorder(BorderFactory.createLineBorder(Color.black));
//        ratioLabel.setBorder(BorderFactory.createLineBorder(Color.black));
//        doorCountLabel.setBorder(BorderFactory.createLineBorder(Color.black));
        timeLabel.setBackground(Color.white);
        ratioLabel.setBackground(Color.white);
        doorCountLabel.setBackground(Color.green);
        timeLabel.setOpaque(true);
        ratioLabel.setOpaque(true);
        doorCountLabel.setOpaque(true);
        //totalTimeLabel.setBorder(BorderFactory.createLineBorder(Color.black));

//        JButton resetButton = new JButton ("Reset");
//        resetButton.setPreferredSize(new Dimension(80, 40));
//        resetButton.addActionListener(new ActionListener()
//        {
//            public void actionPerformed (ActionEvent e)
//            {
//                
//                timeIntoCase = 0;
//                totalTimeOpened = 0;
//                doorCount = 0;
//                ratio = 0;
//                caseStartTime = System.currentTimeMillis();
//                MonitOR.updateData1(totalTimeOpened, myDataset1);
//                MonitOR.updateData2(timeIntoCase, totalTimeOpened, myDataset2);
//                MonitOR.updateLables(totalTimeLabel, doorCountLabel, ratioLabel);
//            }
//        });
        
        //Update time starts here!!!
        ActionListener updateTime = new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                timeIntoCase = ((System.currentTimeMillis() - caseStartTime) / 1000.00);
                
                timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date time = new Date((long)(timeIntoCase*1000));
                formattedCaseTime = timeFormat.format(time);
                timeLabel.setText("<html>Case Time:<br>" + formattedCaseTime+"</html>");  
                ratio = (totalTimeOpened/timeIntoCase) * 100.00;
                MonitOR.updateData2(timeIntoCase, totalTimeOpened, myDataset2);
                MonitOR.updateLables(doorCountLabel, ratioLabel);
                
            }
        };
        new Timer (1000, updateTime).start();
        //Update Time ends here!!!
        
        
        
        
        
        //infoPanel.add(totalTimeLabel);
        infoPanel2.add(timeLabel);
        //infoPanel.add(resetButton);
        infoPanel.add(doorCountLabel);
        infoPanel2.add(ratioLabel);
        return myPanel;
        
        
    }
    private static void showGUI()
    {
        JFrame frame = new JFrame ("BarGraph");
        frame.setUndecorated(true);
        
        MonitOR  test = new MonitOR ();
        frame.setContentPane(test.gui());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize (320,240);
        frame.setVisible (true);
    }
//    public static CategoryDataset updateData1 (double doorTime, DefaultCategoryDataset dataSet)
//    {
//        dataSet.clear();
//        
//        dataSet.addValue (doorTime, "", "DoorTime");
//        return dataSet;
//    }
    public static CategoryDataset updateData2 (double caseTime, double doorTime, DefaultCategoryDataset dataSet)
    {
        dataSet.clear();
        
        
        dataSet.addValue ((doorTime/caseTime) * 100, "", "Percentage");
        return dataSet;
    }
    public static void updateLables (JLabel doorCountLabel, JLabel ratioLabel)
    {
        //totalTimeLabel.setText("Total:" + String.format ("%.1f",totalTimeOpened));
        doorCountLabel.setText("Opened " + doorCount + " times for "+ String.format ("%.1f",totalTimeOpened) + " sec.");
        ratioLabel.setText("<html>% Opened:" + String.format("<br>%.2f", ratio)  + "%</html>");
        if (ratio > 25.00)
        {
            doorCountLabel.setBackground(Color.red);
        }
        else if (ratio > 15.00 && ratio < 25.00)
        {
            doorCountLabel.setBackground(Color.yellow);
        }
        else
        {
            doorCountLabel.setBackground(Color.green);
        }
    }
 

 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws UnknownHostException, IOException, ConnectException {
        String sensorName = "Sensor72";
        boolean done = false;
        boolean connected = false;
        boolean prevIsOpen = false;
        double startTime = 0;
        double stopTime = 0;
        int entryCount = 0;
        File myData = new File ("LocalData.txt");
        File savedData = new File ("SavedData.txt");
        FileWriter mySecondWriter = new FileWriter (savedData, true);
        FileWriter myWriter = new FileWriter(myData, true);
        BufferedWriter outputFileWriter = null;
        BufferedWriter secondFileWriter = null;
        
        SwingUtilities.invokeLater(new Runnable()
                {
                    public void run()
                    {
                        showGUI();
                    }
                });
        
        
     
        final GpioController gpio = GpioFactory.getInstance();
        
        GpioPinDigitalInput mySensor = gpio.provisionDigitalInputPin (RaspiPin.GPIO_01);
        
        //Physical button reset
        GpioPinDigitalInput resetButton = gpio.provisionDigitalInputPin (RaspiPin.GPIO_00, PinPullResistance.PULL_UP);
        resetButton.addListener(new GpioPinListenerDigital() {
            public void handleGpioPinDigitalStateChangeEvent (GpioPinDigitalStateChangeEvent event) {
                timeIntoCase = 0;
                totalTimeOpened = 0;
                doorCount = 0;
                ratio = 0;
                caseStartTime = System.currentTimeMillis();
                //MonitOR.updateData1(totalTimeOpened, myDataset1);
                MonitOR.updateData2(timeIntoCase, totalTimeOpened, myDataset2);
                MonitOR.updateLables(doorCountLabel, ratioLabel);
            }
        });
        
        caseStartTime = System.currentTimeMillis();
        do 
        {
            boolean mySensorState = mySensor.isHigh();
          
            if (!mySensorState)
            {
                if (!prevIsOpen)
                {
                    prevIsOpen = true;
                    startTime = System.currentTimeMillis();
                    System.out.println ("Door Open!");
                }
            }
            else
            {
                if (prevIsOpen)
                {
                    prevIsOpen = false;
                    stopTime = System.currentTimeMillis();
                    double totalTime = (stopTime - startTime) / 1000.00;
                    timeIntoCase = ((stopTime - caseStartTime) / 1000.00);
                    totalTimeOpened = (totalTimeOpened + totalTime);
                    ratio = (totalTimeOpened/timeIntoCase) * 100.00;
                    System.out.println ("Door Closed!");
                    System.out.println("Time: " + totalTime);
                    System.out.println("Total Time: " + totalTimeOpened);
                    System.out.println("Case Time: " + timeIntoCase);
                    
                    String data = "Name: " + sensorName + " Total time: " + totalTime;
                    doorCount ++;
                    //MonitOR .updateData1(totalTimeOpened, myDataset1);
                    MonitOR.updateData2(timeIntoCase, totalTimeOpened, myDataset2);
                    MonitOR.updateLables(doorCountLabel, ratioLabel);
                    
                    
                    try
                    {
                        System.out.println ("Logging data locally...");
                        outputFileWriter = new BufferedWriter(myWriter);
                        secondFileWriter = new BufferedWriter (mySecondWriter);
                        java.util.Date date = new java.util.Date();
                        Timestamp currentTimestamp = new Timestamp(date.getTime());
                        outputFileWriter.write(data + " Timestamp: " + currentTimestamp);
                        outputFileWriter.newLine();
                        outputFileWriter.flush();
                        secondFileWriter.write(data + " Timestamp: " + currentTimestamp);
                        secondFileWriter.newLine();
                        secondFileWriter.flush();
                        
                        entryCount++;
                    }
                    catch (IOException e)
                    {
                        System.err.println("IOException!!!");
                    }
                    if (entryCount >= 5)
                    {
                        System.out.println ("Count at 5 or more");
                        String toServer = "";
                        try
                        {
                            toServer = new Scanner (myData).useDelimiter("\\Z").next();

                        }
                        catch (FileNotFoundException e)
                        {

                            System.err.println ("Unable to locate file.");
                        }
                        FileWriter eraser = new FileWriter (myData);
                        Thread myThread = new Thread (new WriteData(toServer));
                        myThread.start();
                        eraser.write("");
                        entryCount = 0;
                        eraser.close();
   
                    }
                    

                    
                }
            }
        }
        while (!done);
    }
    
}


