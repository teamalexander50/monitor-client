package ui;

import core.Main;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.data.category.DefaultCategoryDataset;
import util.tasks.CaseTimeTask;
import util.tasks.PercentageTask;
import util.TimeMath;

import javax.swing.*;
import java.awt.*;
import java.util.Timer;

/**
 * Handles the User Interface for the Sensor Touchscreen
 */
public class SensorUI
{
	private static final int TOUCHSCREEN_WIDTH = 320;
	private static final int TOUCHSCREEN_HEIGHT = 240;

	private static final int BAR_CHART_WIDTH = 160;
	private static final int BAR_CHART_HEIGHT = 190;

	private Timer caseTimeTimer;
	private DefaultCategoryDataset dataset;
	private JFrame frame;

	private JLabel caseTimeLabel;
	private JLabel totalOpenCountLabel;
	private JLabel totalOpenTimeLabel;

	public SensorUI()
	{
		this.caseTimeTimer = new Timer(false);
		this.dataset = new DefaultCategoryDataset();
		this.frame = new JFrame("Sensor UI");
		this.frame.setUndecorated(true);
		this.frame.setSize(new Dimension(TOUCHSCREEN_WIDTH, TOUCHSCREEN_HEIGHT));
		this.frame.setResizable(false);
		this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.frame.setLocation(dim.width / 2 - this.frame.getSize().width / 2, dim.height / 2 - this.frame.getSize().height / 2);
		this.frame.setContentPane(this.initializePanel());
		this.frame.setVisible(false);
	}

	private JPanel initializePanel()
	{
		JPanel panel = new JPanel(new BorderLayout());
		panel.setPreferredSize(new Dimension(TOUCHSCREEN_WIDTH, TOUCHSCREEN_HEIGHT));
		panel.add(this.initializeInformationPanel(), BorderLayout.LINE_START);
		panel.add(this.bottomBarPanel(), BorderLayout.PAGE_END);
		panel.add(this.initializeBarChart(), BorderLayout.LINE_END);
		return panel;
	}

	private JPanel initializeInformationPanel()
	{
		JPanel informationPanel = new JPanel(new GridLayout(4, 1));
		informationPanel.setPreferredSize(new Dimension(160, 190));

		JLabel timeLabelHeader = new JLabel("Case Time:", JLabel.CENTER);
		timeLabelHeader.setFont(new Font("Serif", Font.BOLD, 24));
		timeLabelHeader.setBackground(Color.WHITE);
		timeLabelHeader.setOpaque(true);

		this.caseTimeLabel = new JLabel("", JLabel.CENTER);
		this.caseTimeLabel.setFont(new Font("Serif", Font.PLAIN, 24));
		this.caseTimeLabel.setBackground(Color.WHITE);
		this.caseTimeLabel.setOpaque(true);

		JLabel percentageLabelHeader = new JLabel("% Opened:", JLabel.CENTER);
		percentageLabelHeader.setFont(new Font("Serif", Font.BOLD, 24));
		percentageLabelHeader.setBackground(Color.WHITE);
		percentageLabelHeader.setOpaque(true);

		JLabel openPercentageLabel = new JLabel("", JLabel.CENTER);
		openPercentageLabel.setFont(new Font("Serif", Font.PLAIN, 24));
		openPercentageLabel.setBackground(Color.WHITE);
		openPercentageLabel.setOpaque(true);

		informationPanel.add(timeLabelHeader);
		informationPanel.add(this.caseTimeLabel);
		informationPanel.add(percentageLabelHeader);
		informationPanel.add(openPercentageLabel);

		this.caseTimeTimer.scheduleAtFixedRate(new CaseTimeTask(Main.getSensor(), this.caseTimeLabel), 0, 1000);
		this.caseTimeTimer.scheduleAtFixedRate(new PercentageTask(Main.getSensor(), openPercentageLabel, this.dataset), 0, 1000);

		return informationPanel;
	}

	private JPanel bottomBarPanel()
	{
		JPanel bottomBarPanel = new JPanel(new GridLayout(2, 1));
		bottomBarPanel.setPreferredSize(new Dimension(320, 50));

		int openCount = Main.getSensor().getTotalOpenings();

		JLabel openedCountLabel = new JLabel(String.format("%d Total Openings", openCount), JLabel.CENTER);
		openedCountLabel.setFont(new Font("Serif", Font.PLAIN, 18));
		openedCountLabel.setBackground(Color.GREEN);
		openedCountLabel.setOpaque(true);

		JLabel openedTimeLabel = new JLabel("00h 00m 00s Total", JLabel.CENTER);
		openedTimeLabel.setFont(new Font("Serif", Font.PLAIN, 18));
		openedTimeLabel.setBackground(Color.GREEN);
		openedTimeLabel.setOpaque(true);

		this.totalOpenCountLabel = openedCountLabel;
		this.totalOpenTimeLabel = openedTimeLabel;

		bottomBarPanel.add(openedCountLabel);
		bottomBarPanel.add(openedTimeLabel);

		return bottomBarPanel;
	}

	private ChartPanel initializeBarChart()
	{

		JFreeChart barChart = ChartFactory.createBarChart("", "", "", this.dataset);
		ChartPanel barChartPanel = new ChartPanel(barChart);
		barChartPanel.setPreferredSize(new Dimension(BAR_CHART_WIDTH, BAR_CHART_HEIGHT));

		CategoryPlot categoryPlot = barChart.getCategoryPlot();
		categoryPlot.setRenderer(new PercentageBarRenderer());
		categoryPlot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);
		NumberAxis range = (NumberAxis)categoryPlot.getRangeAxis();
		range.setRange(0, 100);
		barChart.removeLegend();

		return barChartPanel;
	}

	public void show()
	{
		this.frame.setVisible(true);
	}

	public void openUpdate()
	{
		this.totalOpenCountLabel.setText(String.format("%d Total Openings", Main.getSensor().getTotalOpenings()));
	}

	public void closeUpdate()
	{
		this.dataset.addValue(Main.getSensor().getTotalOpenPercentage(), "", "");
		this.totalOpenTimeLabel.setText(TimeMath.durationToTimeStamp(Main.getSensor().getTotalOpenTime()) + " Total");
	}

	public void reset()
	{
		this.caseTimeLabel.setText(String.format("%02dh %02dm %02ds", 0, 0, 0));
		this.totalOpenCountLabel.setText(String.format("%d Total Openings", 0));
		this.totalOpenTimeLabel.setText(String.format("%.04f", 0.0));
		this.dataset.clear();
	}

}