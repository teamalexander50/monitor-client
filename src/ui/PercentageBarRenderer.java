package ui;

import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;

import java.awt.*;

/**
 * JFreeChart Bar Plot Renderer used to render the percentage bar chart
 * for the Sensor UI
 */
public class PercentageBarRenderer extends BarRenderer
{

	private final double GREEN_CEILING = 0.05;
	private final double YELLOW_CEILING = 0.15;

	public Paint getItemPaint(int row, int col)
	{
		CategoryDataset dataset = this.getPlot().getDataset();
		String rowKey = (String)dataset.getRowKey(row);
		String colKey = (String)dataset.getColumnKey(col);
		double percentage = dataset.getValue(rowKey, colKey).doubleValue();

		if (percentage < this.GREEN_CEILING) {
			return Color.GREEN;
		} else if (percentage < this.YELLOW_CEILING) {
			return Color.YELLOW;
		} else {
			return Color.RED;
		}
	}
}