package networking;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles backlogging of sensor data
 */
public class BacklogManager
{
	/* Path for the backlog file */
	private String backlogFilePath;

	/**
	 * Creates a BacklogManager with the specified file path. The file path should be
	 * a fully-qualified, absolute file path. Relative file paths are discouraged and
	 * may result in undesired behavior.
	 *
	 * @param filePath Absolute path to backlog file
	 */
	public BacklogManager(String filePath)
	{
		this.backlogFilePath = filePath;
	}

	/**
	 * Returns true if the backlog file contains data
	 *
	 * @return True if the backlog file contains data
	 */
	public boolean hasBacklog()
	{
		try {
			return Files.readAllLines(Paths.get(this.backlogFilePath)).size() != 1;
		} catch (IOException ioException) {
			return false;
		}
	}

	/**
	 * Returns a list containing all backlog data points
	 *
	 * @return List containing all backlog data points
	 */
	private List<String> readBacklogLines()
	{
		try {
			return Files.readAllLines(Paths.get(this.backlogFilePath));
		} catch (IOException ioException) {
			return new ArrayList<>();
		}
	}

	/**
	 * Processes the backlog data file into a List of NameValuePairs suitable
	 * for transmission with a ServerConnectionManager.
	 *
	 * @return List containing mutiple Lists containing all NameValuePairs associated with a backlog data point suitable
	 * for transmission with a ServerConnectionManager
	 */
	public List<List<NameValuePair>> process()
	{

		ArrayList<List<NameValuePair>> allParameters = new ArrayList<>();

		if (!this.hasBacklog()) {
			return new ArrayList<>();
		}

		ArrayList<String> backlogLines = new ArrayList<>(this.readBacklogLines());
		String[] backlogLineHeaders = backlogLines.get(0).split(",");
		for (int i = 1; i < backlogLines.size(); i++) {
			String[] backlogLineComponents = backlogLines.get(i).split(",");
			ArrayList<NameValuePair> backlogLineParameters = new ArrayList<>();
			for (int j = 0; j < backlogLineComponents.length; j++) {
				String backlogLineHeader = backlogLineHeaders[j].trim();
				String backlogLineComponent = backlogLineComponents[j];
				backlogLineParameters.add(new BasicNameValuePair(backlogLineHeader, backlogLineComponent));
			}
			allParameters.add(backlogLineParameters);
		}
		return allParameters;
	}

	/**
	 * Clears the backlog file of all data
	 */
	public void clearBacklog()
	{
		if (this.hasBacklog()) {
			try {
				Files.deleteIfExists(Paths.get(this.backlogFilePath));
			} catch (IOException ioException) {
				// Silence
			}
		}
	}
}