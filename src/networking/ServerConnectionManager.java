package networking;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import util.output.FileWriter;
import util.output.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Handles all connections to the server including posting data
 * as well as the connectivity status indicator.
 *
 * Uses the Apache HttpComponents library for all networking.
 */
public class ServerConnectionManager
{
	/* Used to send all HTTP requests */
	private final HttpClient httpClient = HttpClients.createDefault();

	/* Server POST URL */
	private String url;

	/* Writes to local backlog file */
	private FileWriter backlogFileWriter;

	private Logger logger;

	/* Debugging flag specifying if HTTP Requests should be executed */
	private static final boolean shouldExecuteRequests = true;

	/**
	 * Creates a ServerConnectionManager to handle all connections to and from a remote server on multiple ports
	 *
	 * @param url Server Post URL
	 * @param backlogFileWriter Writer to log information to local log file
	 */
	public ServerConnectionManager(String url, FileWriter backlogFileWriter, Logger logger)
	{
		this.url = url;
		this.backlogFileWriter = backlogFileWriter;
		this.logger = logger;
	}

	/**
	 * Executes a POST request, builds the response string, and logs the response to the console. If the POST fails,
	 * the failure reason is logged to the console and false is returned.
	 *
	 * @param post POST Request to Execute
	 * @return True if the POST returns a success status code
	 * @throws IOException If a Network error occurs
	 */
	private boolean executePost(HttpPost post) throws IOException
	{
		if (shouldExecuteRequests) {
			if (this.logger != null) {
				this.logger.log(post.toString(), true, true);
			}
			HttpResponse response = this.httpClient.execute(post);
			if (this.isPostSuccessful(response)) {
				HttpEntity responseEntity = response.getEntity();
				if (responseEntity != null) {
					StringBuilder responseBuilder = new StringBuilder();
					BufferedReader responseReader = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
					String line;
					while ((line = responseReader.readLine()) != null) {
						responseBuilder.append(line);
					}
					if (this.logger != null) {
						this.logger.log(responseBuilder.toString(), true, true);
					}
					responseReader.close();
					return true;
				}
			} else {
				throw new IOException(response.getStatusLine().toString());
			}
		}
		return false;
	}

	/**
	 * Sends an HTTP Post request to the specified URL constant containing data and
	 * prints the server response to the console.
	 * Returns true if the server responds with a status code of 200 OK.
	 * Returns false otherwise.
	 *
	 * Note: dataParameters should be set up according to the Apache Http-Components
	 * methodology. This requires generating a list of name-value pairs which will be
	 * encoded into a parametrized URL.
	 *
	 * Note: The POST request is sent to the Data URL
	 *
	 * For example: After setting up the name-value pairs (id, "1"), (time, "t"), and (duration, "d"),
	 * the POST URL will effectively be http://site.domain/id=1&time=t&duration=d
	 *
	 * @param postParameters Data to be sent to the server
	 * @return True if the server responds with a status code 200
	 * @throws IOException If an error occurs while writing to the local backlog file
	 */
	public boolean post(List<NameValuePair> postParameters, boolean shouldBacklog) throws IOException
	{
		try {
			HttpPost post = new HttpPost(this.url);
			HttpEntity dataParametersEntity = new UrlEncodedFormEntity(postParameters, "UTF-8");
			post.setEntity(dataParametersEntity);
			return this.executePost(post);
		} catch (UnsupportedEncodingException unsupportedEncodingException) {
			if (this.logger != null) {
				this.logger.log(unsupportedEncodingException.toString(), true, true);
			}
		} catch (IOException httpIOException) {
			this.logger.log(httpIOException.toString(), true, true);
			if (shouldBacklog && this.backlogFileWriter != null) {
				if (this.logger != null) {
					this.logger.log("Saving to backlog at " + this.backlogFileWriter.getFilePath(), true, true);
				}
				this.backlogFileWriter.writeData(postParameters);
			}
			throw httpIOException;
		}
		return false;
	}

	/**
	 * Returns true if the HTTP Response contains a success status code
	 *
	 * @param response HTTP Response
	 * @return True if success status code
	 */
	private boolean isPostSuccessful(HttpResponse response)
	{
		int statusCode = response.getStatusLine().getStatusCode();
		return statusCode >= 200 && statusCode < 300;
	}

	/**
	 * Returns the file path to the backlog file
	 *
	 * @return Path to the backlog file
	 */
	public String getBacklogFilePath()
	{
		return this.backlogFileWriter.getFilePath();
	}
}
