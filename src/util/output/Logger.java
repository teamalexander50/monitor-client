package util.output;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Seamlessly log messages to System console and log file simultaneously
 */
public class Logger
{
	private FileWriter fileWriter;

	/**
	 * Creates a Logger that will log all messages to the specified file path.
	 *
	 * @param logFilePath Absolute path to log file
	 */
	public Logger(String logFilePath)
	{
		this.fileWriter = new FileWriter(logFilePath);
	}

	/**
	 * Logs a String with a timestamp. Example format:
	 *
	 * [18:30:00, 3/25/2016] Message
	 *
	 *
	 * @param message Message to be logged
	 * @param toConsole True if the message should be printed to System out
	 * @param toLogFile True if the message should be written to the log file
	 */
	public void log(String message, boolean toConsole, boolean toLogFile)
	{
		LocalDateTime now = LocalDateTime.now();
		message = "[" + now.format(DateTimeFormatter.ofPattern("HH:mm:ss, MM/dd/YYYY")) + "] " + message;

		if (toConsole) {
			System.out.println(message);
		}

		if (toLogFile && this.fileWriter != null) {
			try {
				this.fileWriter.writeString(message);
			} catch (IOException ioException) {
				ioException.printStackTrace();
				System.out.println("Error writing to log file " + this.fileWriter.getFilePath());
			}
		}
	}

	/**
	 * Logs an Exception stack trace with a timestamp. Example format:
	 *
	 * [18:30:00, 3/25/2016] Exception Stack Trace:
	 * java.net.SocketException: Connection reset
	 *
	 * @param e Exception for which the stack trace should be logged
	 * @param toConsole True if the message should be printed to System out
	 * @param toLogFile True if the message should be written to the log file
	 */
	public void logStackTrace(Exception e, boolean toConsole, boolean toLogFile)
	{
		LocalDateTime now = LocalDateTime.now();
		String timestamp = "[" + now.format(DateTimeFormatter.ofPattern("HH:mm:ss, MM/dd/YYYY")) + "]";

		if (toConsole) {
			System.out.println(timestamp + " Exception Stack Trace:");
			e.printStackTrace();
		}

		if (toLogFile && this.fileWriter != null) {
			try {
				this.fileWriter.writeString(timestamp + " Exception Stack Trace:");
				this.fileWriter.writeExceptionStackTrace(e);
			} catch (IOException ioException) {
				ioException.printStackTrace();
				System.out.println("Error writing to log file " + this.fileWriter.getFilePath());
			}
		}
	}

}
