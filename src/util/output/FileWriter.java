package util.output;

import org.apache.http.NameValuePair;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

/**
 * Handles all file output
 */
public class FileWriter
{
	/* Path to local file */
	private Path filePath;

	/**
	 * Used to write to a local backlog file. This class is intended to be used in conjunction with the
	 * ServerConnectionManager to log failed POST requests.
	 *
	 * @param filePathString Path to local file
	 */
	public FileWriter(String filePathString)
	{
		this.filePath = Paths.get(filePathString);
	}

	/**
	 * Ensures that the target file exists. If the file does not exist, it is created
	 *
	 * @throws IOException If an error occurs reading or creating the file
	 */
	private void checkFileExists() throws IOException
	{
		if (!Files.exists(this.filePath)) {
			Files.createFile(this.filePath);
		}
	}

	/**
	 * Returns the number of lines of data in the local backlog file.
	 * Note: column titles line is excluded from the count
	 *
	 * @return Number of data lines in the local backlog file
	 * @throws IOException If error occurs opening local backlog file
	 */
	private int getLineCount() throws IOException
	{
		return Files.readAllLines(this.filePath).size();
	}

	/**
	 * Writes a header line containing the backlog column names to the local backlog file.
	 * Note: the column names correlate to the key of the NameValuePair objects.
	 *
	 * @param dataPairs List of NameValuePair objects containing the column names to be written
	 * @throws IOException If error occurs opening or writing to the local backlog file
	 */
	private void writeHeader(List<NameValuePair> dataPairs) throws IOException
	{
		String line = "";
		for (NameValuePair pair : dataPairs) {
			line += pair.getName() + ", ";
		}
		line = line.substring(0, line.length()-2) + "\n";
		Files.write(this.filePath, line.getBytes(), StandardOpenOption.APPEND);
	}

	/**
	 * Writes a line of data to the local backlog file. The method accepts a
	 * List of NameValuePair objects to mesh with the ServerConnectionManager.
	 * The method checks if the local backlog is empty and writes the backlog column names if so.
	 *
	 * @param dataPairs List of NameValuePair objects containing the data to be written
	 * @throws IOException If error occurs opening or writing to the local backlog file
	 */
	public void writeData(List<NameValuePair> dataPairs) throws IOException
	{
		this.checkFileExists();

		if (this.getLineCount() == 0) {
			this.writeHeader(dataPairs);
		}

		String line = "";
		for (NameValuePair pair : dataPairs) {
			line += pair.getValue() + ", ";
		}
		line = line.substring(0, line.length() - 2) + "\n";
		Files.write(this.filePath, line.getBytes(), StandardOpenOption.APPEND);
	}

	/**
	 * Writes an exception stack trace to the target file
	 *
	 * @param exception Exception for which the stack trace should be written
	 * @throws IOException If an error occurs while writing to the target file
	 */
	public void writeExceptionStackTrace(Exception exception) throws IOException
	{
		this.checkFileExists();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		PrintStream stackTraceStream = new PrintStream(outputStream);
		exception.printStackTrace(stackTraceStream);
		Files.write(this.filePath, outputStream.toString().getBytes(), StandardOpenOption.APPEND);
	}

	/**
	 * Writes a string to the target file. If the last character is not a newline, the newline
	 * character is appended automatically.
	 *
	 * @param string String to be written
	 * @throws IOException If an error occurs while writing to the target file
	 */
	public void writeString(String string) throws IOException
	{
		if (string.charAt(string.length() - 1) != '\n') {
			string += "\n";
		}
		this.checkFileExists();
		Files.write(this.filePath, string.getBytes(), StandardOpenOption.APPEND);
	}

	/**
	 * Returns the absolute path to the target file
	 *
	 * @return Absolute path to the target file
	 */
	public String getFilePath()
	{
		return this.filePath.toAbsolutePath().toString();
	}
}
