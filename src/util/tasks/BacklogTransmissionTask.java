package util.tasks;

import core.Sensor;
import networking.BacklogManager;
import networking.ServerConnectionManager;
import org.apache.http.NameValuePair;
import util.output.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

/**
 * Timer Task subclass used to transmit backlog data to data server
 */
public class BacklogTransmissionTask extends TimerTask
{
	private Sensor sensor;
	private BacklogManager backlogManager;
	private ServerConnectionManager dataConnectionManager;
	private Logger logger;

	/**
	 * Creates a BacklogTransmissionTask with an associated Sensor object, a ServerConnectionManager
	 * for transmission, and a Logger for message logging
	 *
	 * @param sensor Sensor with which the data should be associated
	 * @param dataConnectionManager ServerConnectionManager for transmission
	 * @param logger Logger for message logging
	 */
	public BacklogTransmissionTask(Sensor sensor, ServerConnectionManager dataConnectionManager, Logger logger)
	{
		this.sensor = sensor;
		this.backlogManager = new BacklogManager(dataConnectionManager.getBacklogFilePath());
		this.dataConnectionManager = dataConnectionManager;
		this.logger = logger;
	}

	/**
	 * Triggered by the Timer object.
	 * Reads all backlog data and attempts to transmit the data to the server using the ServerConnectionManager.
	 * If the backlog data is transmitted successfully, the backlog file is cleared. If transmission was unsuccessful,
	 * the backlog is unchanged.
	 */
	@Override
	public void run()
	{
		if (this.sensor.isOnline()) {
			if (this.backlogManager.hasBacklog()) {
				try {
					this.logger.log("Attempting to Transmit Backlog", true, true);
					ArrayList<List<NameValuePair>> allBacklogParameters = new ArrayList<>(this.backlogManager.process());
					for (int i = 0; i < allBacklogParameters.size(); i++) {
						if (this.dataConnectionManager.post(allBacklogParameters.get(i), false)) {
							this.logger.log(String.format("%d of %d Transmitted", i+1, allBacklogParameters.size()), true, true);
						}
					}
					this.logger.log("Backlog Transmitted", true, true);
					this.backlogManager.clearBacklog();
					this.logger.log("Backlog Cleared", true, true);
				} catch (IOException ioException) {
					this.logger.log(ioException.toString(), true, true);
				}
			} else {
				this.logger.log("No Backlog", true, true);
			}
		} else {
			this.logger.log("Sensor is Offline", true, true);
		}
	}
}
