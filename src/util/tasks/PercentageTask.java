package util.tasks;

import core.Sensor;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import java.util.TimerTask;

/**
 * Timer Task subclass used to updated the GUI percentage label
 */
public class PercentageTask extends TimerTask
{

	private Sensor sensor;
	private DefaultCategoryDataset dataset;
	private JLabel label;

	/**
	 * Creates a PercentageTask with an associated Sensor object, a JLabel that should be updated
	 * with the calculated percentage, and a JFreeChart dataset to update with the calculated percentage
	 *
	 * @param sensor Associated Sensor object
	 * @param label Label that should be updated with the calculated percentage
	 * @param percentageDataset Dataset to be updated with the calculated percentage
	 */
	public PercentageTask(Sensor sensor, JLabel label, DefaultCategoryDataset percentageDataset)
	{
		this.sensor = sensor;
		this.dataset = percentageDataset;
		this.label = label;
	}

	/**
	 * Triggered by the Timer object.
	 * Retrieves the percentage of time that the door has been opened by the associated Sensor. The percentage is
	 * formatted and displayed inside the label
	 */
	@Override
	public void run()
	{
		this.dataset.clear();
		this.dataset.addValue(this.sensor.getTotalOpenPercentage(), "", "");
		label.setText(String.format("%.04f", this.sensor.getTotalOpenPercentage()));
	}
}
