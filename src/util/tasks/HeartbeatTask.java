package util.tasks;

import core.Sensor;
import networking.ServerConnectionManager;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import util.output.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

/**
 * Timer Task subclass used to send a Heartbeat to the Heartbeat Server
 */
public class HeartbeatTask extends TimerTask
{
	private Sensor sensor;
	private ServerConnectionManager heartbeatServerConnectionManager;
	private Logger logger;

	/**
	 * Creates a HeartbeatTask with an associated Sensor object, a ServerConnectionManager for transmission, and a
	 * Logger for message logging.
	 *
	 * @param sensor Sensor with which the data should be associated
	 * @param heartbeatServerConnectionManager ServerConnectionManager for transmission
	 * @param logger Logger for message logging
	 */
	public HeartbeatTask(Sensor sensor, ServerConnectionManager heartbeatServerConnectionManager, Logger logger)
	{
		this.sensor = sensor;
		this.heartbeatServerConnectionManager = heartbeatServerConnectionManager;
		this.logger = logger;
	}

	/**
	 * Triggered by the Timer object.
	 * Sends a heartbeat off to the Heartbeat Server using the ServerConnectionManager. If an exception is caught,
	 * it is logged by the logger.
	 */
	@Override
	public void run()
	{
		try {
			List<NameValuePair> heartbeatParameters = new ArrayList<>();
			heartbeatParameters.add(new BasicNameValuePair("or_id", String.valueOf(this.sensor.getOperatingRoomID())));
			heartbeatParameters.add(new BasicNameValuePair("sensor_id", String.valueOf(this.sensor.getSensorID())));
			this.sensor.setOnline(this.heartbeatServerConnectionManager.post(heartbeatParameters, false));
		} catch (IOException ioException) {
			this.logger.log(ioException.toString(), true, true);
		}
	}
}
