package util.tasks;

import core.Sensor;
import util.TimeMath;

import javax.swing.*;
import java.time.LocalDateTime;
import java.util.TimerTask;

/**
 * Timer Task subclass used to update the GUI case time label
 */
public class CaseTimeTask extends TimerTask
{
	private Sensor sensor;
	private JLabel label;

	/**
	 * Creates a CaseTimeTask with an associated Sensor object and a JLabel that should be updated
	 * with the current case time
	 *
	 * @param sensor Associated Sensor object
	 * @param label Label that should be updated with the current case time
	 */
	public CaseTimeTask(Sensor sensor, JLabel label)
	{
		this.sensor = sensor;
		this.label = label;
	}

	/**
	 * Triggered by the Timer object.
	 * Calculates the time difference between Now and the Sensor boot time. The time is
	 * formatted into a timestamp and displayed inside the label
	 */
	@Override
	public void run() {
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime bootTime = this.sensor.getBootTime();
		label.setText(TimeMath.differenceToTimeStamp(now, bootTime));
	}
}
