package util;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

/**
 * Class to handle arithmetic with times
 */
public class TimeMath
{
	/**
	 * Calculates the difference between start time and end time in the specified time unit.
	 * If the difference is negative, the sign is flipped.
	 *
	 * @param start Beginning time
	 * @param end End time
	 * @param unit Unit for the amount to return
	 * @return Total time between start and end in specified unit
	 */
	public static long differenceIn(LocalDateTime start, LocalDateTime end, ChronoUnit unit)
	{
		long difference = start.until(end, unit);
		if (difference < 0) {
			return -difference;
		}
		return difference;
	}

	/**
	 * Calculates the difference between start time and end time and formats the difference into a timestamp
	 *
	 * @param start Beginning time
	 * @param end End time
	 * @return Timestamp for the difference between start time and end time
	 */
	public static String differenceToTimeStamp(LocalDateTime start, LocalDateTime end)
	{
		LocalDateTime tempTime = LocalDateTime.from(end);

		long days = tempTime.until(start, ChronoUnit.DAYS);
		if (days < 0) {
			days = -days;
		}
		tempTime = tempTime.plusDays(days);

		long hours = tempTime.until(start, ChronoUnit.HOURS);
		if (hours < 0) {
			hours = -hours;
		}
		tempTime = tempTime.plusHours(hours);

		long minutes = tempTime.until(start, ChronoUnit.MINUTES);
		if (minutes < 0) {
			minutes = -minutes;
		}
		tempTime = tempTime.plusMinutes(minutes);

		long seconds = tempTime.until(start, ChronoUnit.SECONDS);
		if (seconds < 0) {
			seconds = -seconds;
		}

		return String.format("%02dh %02dm %02ds", hours, minutes, seconds);
	}

	/**
	 * Transforms a duration into a formatted timestamp
	 *
	 * @param duration Duration for which a timestamp should be generated
	 * @return Timestamp of the duration
	 */
	public static String durationToTimeStamp(Duration duration)
	{
		long seconds = duration.getSeconds();

		long days = TimeUnit.SECONDS.toDays(seconds);
		seconds -= TimeUnit.DAYS.toSeconds(days);

		long hours = TimeUnit.SECONDS.toHours(seconds);
		seconds -= TimeUnit.HOURS.toSeconds(hours);

		long minutes = TimeUnit.SECONDS.toMinutes(seconds);
		seconds -= TimeUnit.MINUTES.toSeconds(minutes);

		return String.format("%02dh %02dm %02ds", hours, minutes, seconds);
	}
}
