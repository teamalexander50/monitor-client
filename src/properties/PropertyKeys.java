package properties;

/**
 * Constants
 */
public class PropertyKeys
{
	public static String operatingRoomID = "operating_room_id";
	public static String sensorID = "sensor_id";
	public static String backlogPath = "backlog_path";
	public static String backlogTransmissionFrequency = "backlog_transmission_frequency";
	public static String dataPostUrl = "data_post_url";
	public static String heartbeatPostUrl = "heartbeat_post_url";
	public static String heartbeatFrequency = "heartbeat_frequency";
	public static String logFilePath = "log_file_path";
}
