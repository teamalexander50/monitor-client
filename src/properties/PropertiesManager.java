package properties;

import core.Main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.CodeSource;
import java.util.Properties;

/**
 * Manages the configurable properties file
 */
public class PropertiesManager
{
	private String fileName;

	/**
	 * Creates a Properties Manager for the default config.properties file
	 */
	public PropertiesManager()
	{
		this.fileName = "config.properties";
	}

	/**
	 * Opens the properties file for reading
	 *
	 * @return InputStream for reading the properties file
	 */
	private InputStream openPropertiesFileInputStream() throws IOException
	{
		CodeSource workingDirectorySource = Main.class.getProtectionDomain().getCodeSource();
		URL propertiesFileURL = new URL(workingDirectorySource.getLocation(), this.fileName);
		return new FileInputStream(propertiesFileURL.getFile());
	}

	/**
	 * Returns the value associated with the provided key inside the properties file
	 *
	 * @param key Key to lookup inside properties file
	 * @return Value associated with the specified key
	 * @throws IOException If reading the properties file produces an error
	 */
	private Object readProperty(String key) throws IOException
	{
		Properties properties = new Properties();
		properties.load(this.openPropertiesFileInputStream());
		return properties.getProperty(key);
	}

	/**
	 * Parses value associated with the provided key into a String.
	 *
	 * @param key Key to lookup inside properties file
	 * @return Value, parsed into a String, associated with the specified key
	 * @throws IOException If reading the properties file produces an error
	 */
	public String readStringProperty(String key) throws IOException
	{
		return String.valueOf(this.readProperty(key));
	}

	/**
	 * Parses value associated with the provided key into an integer. If the
	 * value cannot be parsed into an integer, -1 is returned.
	 *
	 * @param key Key to lookup inside properties file
	 * @return Value, parsed into an integer, associated with the specified key; -1 otherwise
	 * @throws IOException If reading the properties file produces an error
	 */
	public int readIntProperty(String key) throws IOException
	{
		try {
			return Integer.parseInt(readStringProperty(key));
		} catch (NumberFormatException e) {
			return -1;
		}
	}
}
