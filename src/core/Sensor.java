package core;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import util.TimeMath;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents an Operating Room Door Sensor
 */
public class Sensor {
	private int operatingRoomID;
    private int sensorID;
	private boolean online;
    private boolean open;
	private LocalDateTime bootTime;
	private LocalDateTime openTime;
    private LocalDateTime closeTime;
	private Duration totalOpenTime;
	private int totalOpenings;

	/**
	 * Creates a new Sensor with an ID and a user-friendly Label
	 *
	 * @param operatingRoomID Unique Operating Room Identifier
	 * @param sensorID Unique Sensor Identifier
	 */
    public Sensor(int operatingRoomID, int sensorID)
    {
	    this.operatingRoomID = operatingRoomID;
        this.sensorID = sensorID;
	    this.online = true;
        this.open = false;
	    this.bootTime = LocalDateTime.now();
        this.openTime = null;
        this.closeTime = null;
        this.totalOpenings = 0;
	    this.totalOpenTime = Duration.ZERO;
    }

	/**
	 * This method is called when the sensor detects the door has been opened.
	 * When the door is opened, the sensor state is set to open, the open time
	 * is logged, and the total openings is incremented.
	 */
	public void registerOpen()
	{
		this.open = true;
		this.openTime = LocalDateTime.now();
		this.totalOpenings++;
	}

	/**
	 * This method is called when the sensor detects the door has been closed.
	 * When the door is closed, the sensor state is set to closed and the close
	 * time is logged.
	 */
	public void registerClose()
	{
		this.open = false;
		this.closeTime = LocalDateTime.now();
		this.totalOpenTime = this.totalOpenTime.plus(this.getDuration());
	}

	/**
	 * Returns the unique identifier for the sensor
	 *
	 * @return Unique identifier for the sensor
	 */
    public int getSensorID()
    {
        return this.sensorID;
    }

	/**
	 * Returns the unique identifier for the Operating Room housing the Sensor
	 *
	 * @return Unique identifier for the Operating Room housing the Sensor
	 */
	public int getOperatingRoomID()
	{
		return this.operatingRoomID;
	}

	/**
	 * Returns the date at which the sensor boots up
	 *
	 * @return Date at which the sensor booted up
	 */
	public LocalDateTime getBootTime()
	{
		return this.bootTime;
	}

	public boolean isOpen()
	{
		return this.open;
	}

	/**
	 * Returns the total duration of time that the door has been registered as open.
	 *
	 * @return Total duration of time that the door has been open
	 */
	public Duration getTotalOpenTime()
	{
		return this.totalOpenTime;
	}

	/**
	 * Returns the duration that the door was last opened
	 * If the door is currently open, the duration will be calculated using the current time.
	 * If the door is not currently open, the duration will be calculated for the last
	 * event (the close time subtracted by the open time)
	 *
	 * @return Duration that the door was last opened
	 */
	public Duration getDuration()
	{
		if (this.open) {
			return Duration.between(this.openTime, LocalDateTime.now());
		}
		return Duration.between(this.openTime, this.closeTime);
	}

	/**
	 * Returns the percentage of time that the door was open relative to the total amount
	 * of time that the sensor has been operational
	 *
	 * @return Percentage of time that the door was open
	 */
	public double getTotalOpenPercentage() {
		double totalMilliseconds = TimeMath.differenceIn(this.bootTime, LocalDateTime.now(), ChronoUnit.MILLIS);
		double totalOpenMilliseconds = new Long(this.totalOpenTime.toMillis()).doubleValue();
		double percentage = (totalOpenMilliseconds / totalMilliseconds) * 100;
		if (Double.isNaN(percentage)) {
			percentage = 0.0;
		}
		return percentage;
	}

	/**
	 * Returns the total number of times that the door has been detected as open
	 *
	 * @return Total number of times that the door has been opened
	 */
    public int getTotalOpenings()
    {
        return this.totalOpenings;
    }

	public boolean isOnline()
	{
		return this.online;
	}

	/**
	 * Sets the sensor online status
	 *
	 * @param online True if the sensor is connected to the network
	 */
	public void setOnline(boolean online)
	{
		this.online = online;
	}

	/**
	 * Sets the Sensor back to a default state.
	 */
	public void reset()
	{
		this.open = false;
		this.bootTime = LocalDateTime.now();
		this.openTime = null;
		this.closeTime = null;
		this.totalOpenings = 0;
		this.totalOpenTime = Duration.ZERO;
		this.online = true;
	}

	/**
	 * Returns a list of name-value pairs used by the Apache HTTPClient to generate
	 * a parametrized URL. Note that this method should only be used when working with
	 * Apache HTTP Components.
	 *
	 * The return value should be used to create an UrlEncodedFormEntity object with
	 * UTF-8 encoding. The UrlEncodedFormEntity can then be set as the HTTPPost
	 * entity.
	 *
	 * @return List of name-value pairs to generate a parametrized URL for the Apache HTTPClient
	 */
    public List<NameValuePair> postParameterPairs()
    {
	    List<NameValuePair> pairs = new ArrayList<>();
	    pairs.add(new BasicNameValuePair("or_id", String.valueOf(this.operatingRoomID)));
	    pairs.add(new BasicNameValuePair("sensor_id", String.valueOf(this.sensorID)));
	    pairs.add(new BasicNameValuePair("open_time", String.valueOf(this.openTime)));
	    pairs.add(new BasicNameValuePair("seconds", String.valueOf(this.getDuration().toMillis() / 1000)));
	    return pairs;
    }
}