package core;

import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import networking.ServerConnectionManager;
import properties.PropertiesManager;
import properties.PropertyKeys;
import ui.SensorUI;
import util.output.FileWriter;
import util.output.Logger;
import util.tasks.BacklogTransmissionTask;
import util.tasks.HeartbeatTask;

import javax.swing.*;
import java.io.IOException;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Entry Point for the Application
 */
public class Main
{
	private static Logger logger;

	private static Sensor sensor;
	private static SensorUI sensorUI;

	private static ServerConnectionManager dataServerConnectionManager;
	private static ServerConnectionManager heartbeatServerConnectionManager;

	private static GpioPinDigitalInput resetButton;
	private static GpioPinDigitalInput magneticReedSwitch;

	/* True if Client should use hardware magnetic reed switch */
	private static final boolean shouldUseHardware = true;

	/* True if Client should transmit data to server */
	private static final boolean shouldUseNetwork = true;

	/* True if Client should attempt to transmit backlogged data to server */
	private static final boolean shouldTransmitBacklog = true;

	/* True if Client should periodically send a heartbeat to server */
	private static final boolean shouldUseHeartbeat = true;

	/* True if Client should display User Interface */
	private static final boolean shouldUseUI = true;

	/* True if Client should respond to internal command line interface commands */
	private static final boolean shouldUseCommandInterface = false;

	/**
	 * Main Entry Point
	 *
	 * @param args Command Line Arguments
	 */
	public static void main(String[] args) throws IOException
	{
		logger = initializeLogger();
		logger.log("--- Booting ---", false, true);

		try {

			sensor = initializeSensor();

			if (shouldUseHardware) {
				initializeGPIOPins();
				initializeHardwareEventListeners();
			}

			if (shouldUseNetwork) {
				dataServerConnectionManager = initializeDataServerConnectionManager();
				if (shouldTransmitBacklog) {
					initializeBacklogTransmissionTimer();
				}
			}

			if (shouldUseHeartbeat) {
				heartbeatServerConnectionManager = initializeHeartbeatServerConnectionManager();
				initializeHeartbeatTimer();
			}

			if (shouldUseUI) {
				sensorUI = initializeUI();
			}

			if (shouldUseCommandInterface) {
				runCommandInterface();
			}

		} catch (Exception e) {
			System.out.println("Here!!!!");
			logger.log(e.toString(), true, true);
			e.printStackTrace();
		}
	}

	/**
	 * Provides an internal command line interface that allows open, close,
	 * and reset events to be simulated. This is to be used for testing
	 * and debugging.
	 */
	private static void runCommandInterface()
	{
		Scanner scanner = new Scanner(System.in);
		while (true) {
			System.out.print("Command: ");
			String in = scanner.nextLine();
			switch (in.toLowerCase()) {
				case "open":
					if (sensor.isOpen()) {
						logger.log("Error: Sensor must be closed", true, false);
						break;
					}
					openEvent();
					break;
				case "close":
					if (!sensor.isOpen()) {
						logger.log("Error: Sensor must be open", true, false);
						break;
					}
					closeEvent();
					break;
				case "reset":
					if (sensor.isOpen()) {
						logger.log("Error: Sensor must be closed", true, false);
						break;
					}
					resetEvent();
					break;
				case "quit":
					logger.log("Quitting Application", true, false);
					System.exit(0);
					break;
				case "help":
					logger.log("Help:", true, false);
					logger.log("\topen\tSimulate Open Event", true, false);
					logger.log("\tclose\tSimulate Close Event", true, false);
					logger.log("\treset\tSimulate Reset Button", true, false);
					logger.log("\tquit\tQuit Command Interface", true, false);
				default:
					logger.log("Unknown Command. Try Again.", true, false);
			}
		}
	}

	/**
	 * Called when the sensor detects that the door is opened
	 */
	private static void openEvent()
	{
		logger.log("Open Event", true, true);
		sensor.registerOpen();
		if (shouldUseUI) {
			sensorUI.openUpdate();
		}
	}

	/**
	 * Called when the sensor detects that the door is closed
	 */
	private static void closeEvent()
	{
		logger.log("Close Event", true, true);
		sensor.registerClose();
		if (shouldUseUI) {
			sensorUI.closeUpdate();
		}

		if (shouldUseNetwork) {
			try {
				dataServerConnectionManager.post(sensor.postParameterPairs(), true);
			} catch (IOException ioException) {
				logger.log(ioException.toString(), true, true);
			}
		}
	}

	/**
	 * Called when the sensor detects the reset button is pressed
	 */
	private static void resetEvent()
	{
		logger.log("Reset Event", true, true);
		if (!sensor.isOpen()) {
			sensor.reset();

			if (shouldUseUI) {
				sensorUI.reset();
			}
		}
	}

	private static Logger initializeLogger() throws IOException
	{
		PropertiesManager propertiesManager = new PropertiesManager();
		String logFilePath = propertiesManager.readStringProperty(PropertyKeys.logFilePath);
		return new Logger(logFilePath);
	}

	/**
	 * Creates a Sensor object by reading in configuration variables from the properties file
	 *
	 * @return Initialized Sensor object
	 * @throws IOException If PropertiesManager encounters an error
	 */
	private static Sensor initializeSensor() throws IOException
	{
		logger.log("Initializing Sensor", true, true);
		PropertiesManager propertiesManager = new PropertiesManager();
		int orID = propertiesManager.readIntProperty(PropertyKeys.operatingRoomID);
		int sensorID = propertiesManager.readIntProperty(PropertyKeys.sensorID);
		if (orID == -1 || sensorID == -1) {
			throw new IllegalArgumentException("Error reading OR ID or Sensor ID value from config.properties");
		}
		logger.log(String.format("Operating Room ID = %d, Sensor ID = %d", orID, sensorID), true, true);
		return new Sensor(orID, sensorID);
	}

	/**
	 * Creates a ServerConnectionManager object by reading in configuration variables from the properties file.
	 * The created ServerConnectionManager should be used to send door data to the data server.
	 *
	 * @return Initialized ServerConnectionManager object
	 * @throws IOException If PropertiesManager encounters an error
	 */
	private static ServerConnectionManager initializeDataServerConnectionManager() throws IOException
	{
		logger.log("Initializing Data Server Connection Manager", true, true);
		PropertiesManager propertiesManager = new PropertiesManager();
		String dataPostURL = propertiesManager.readStringProperty(PropertyKeys.dataPostUrl);
		FileWriter fileWriter = initializeLocalFileWriter();
		return new ServerConnectionManager(dataPostURL, fileWriter, logger);
	}

	/**
	 * Creates a ServerConnectionManager object by reading in configuration variables from the properties file.
	 * The created ServerConnectionManager should be used to send heartbeat data to the heartbeat server.
	 *
	 * @return Initialized ServerConnectionManager object
	 * @throws IOException If PropertiesManager encounters an error
	 */
	private static ServerConnectionManager initializeHeartbeatServerConnectionManager() throws IOException
	{
		logger.log("Initializing Heartbeat Server Connection Manager", true, true);
		PropertiesManager propertiesManager = new PropertiesManager();
		String heartbeatPostURL = propertiesManager.readStringProperty(PropertyKeys.heartbeatPostUrl);
		FileWriter fileWriter = initializeLocalFileWriter();
		return new ServerConnectionManager(heartbeatPostURL, fileWriter, logger);
	}

	/**
	 * Creates a Timer object by reading in configuration variables from the properties file.
	 * The created Timer is responsible for triggering a HeartbeatTask on a specified delay.
	 *
	 * @throws IOException If PropertiesManager encounters an error
	 */
	private static void initializeHeartbeatTimer() throws IOException
	{
		logger.log("Initializing Heartbeat Timer", true, true);
		PropertiesManager propertiesManager = new PropertiesManager();
		int heartbeatFrequencySeconds = propertiesManager.readIntProperty(PropertyKeys.heartbeatFrequency);
		if (heartbeatFrequencySeconds == -1) {
			throw new IllegalArgumentException("Error reading Heartbeat Frequency value from config.properties");
		}
		TimerTask heartbeatTask = new HeartbeatTask(sensor, heartbeatServerConnectionManager, logger);
		Timer timer = new Timer(false);
		timer.schedule(heartbeatTask, 10*1000, heartbeatFrequencySeconds * 1000);
	}

	/**
	 * Creates a Timer object by reading in configuration variables from the properties file.
	 * The created Timer is responsible for triggering a BacklogTransmissionTask on a specified delay.
	 *
	 * @throws IOException If PropertiesManager encounters an error
	 */
	private static void initializeBacklogTransmissionTimer() throws IOException
	{
		logger.log("Initializing Backlog Transmission Timer", true, true);
		PropertiesManager propertiesManager = new PropertiesManager();
		int backlogTransmissionFrequencySeconds = propertiesManager.readIntProperty(PropertyKeys.backlogTransmissionFrequency);
		if (backlogTransmissionFrequencySeconds == -1) {
			throw new IllegalArgumentException("Error reading Backlog Transmission Frequency value from config.properties");
		}
		TimerTask backlogTransmissionTask = new BacklogTransmissionTask(sensor, dataServerConnectionManager, logger);
		Timer timer = new Timer(false);
		timer.schedule(backlogTransmissionTask, 10*1000, backlogTransmissionFrequencySeconds * 1000);
	}

	/**
	 * Creates a Local File Writer object by reading in configuration variables from the properties file
	 *
	 * @return Initialized Local File Writer object
	 * @throws IOException If PropertiesManager encounters an error
	 */
	private static FileWriter initializeLocalFileWriter() throws IOException
	{
		logger.log("Initializing FileWriter", true, true);
		PropertiesManager propertiesManager = new PropertiesManager();
		String backlogPath = propertiesManager.readStringProperty(PropertyKeys.backlogPath);
		return new FileWriter(backlogPath);
	}

	/**
	 * Creates a SensorUI object and asks java.swing to show the GUI
	 *
	 * @return Initialized SensorUI
	 */
	private static SensorUI initializeUI()
	{
		logger.log("Initializing UI", false, true);
		SensorUI userInterface = new SensorUI();
		SwingUtilities.invokeLater(userInterface::show);
		return userInterface;
	}

	/**
	 * Initializes GPIO Pins for Reset Button and Magnetic Reed Switch
	 */
	private static void initializeGPIOPins()
	{
		logger.log("Initializing GPIO Pins", true, true);
		GpioController gpioController = GpioFactory.getInstance();
		resetButton = gpioController.provisionDigitalInputPin(RaspiPin.GPIO_00, PinPullResistance.PULL_UP);
		magneticReedSwitch = gpioController.provisionDigitalInputPin(RaspiPin.GPIO_01);
	}

	/**
	 * Sets up Event Listeners for All Raspberry Pi GPIO Pins
	 */
	private static void initializeHardwareEventListeners()
	{
		logger.log("Initializing Hardware Listeners", true, true);

		/**
		 * Listener for Magnetic Reed Switch
		 */
		magneticReedSwitch.addListener((GpioPinListenerDigital) event -> {
			if (event.getState().isLow()) {
				openEvent();
			} else {
				closeEvent();
			}
		});

		/**
		 * Listener for Reset Button
		 */
		resetButton.addListener((GpioPinListenerDigital) event -> resetEvent());
	}

	/**
	 * Returns the sensor object
	 *
	 * @return Sensor object
	 */
	public static Sensor getSensor()
	{
		return sensor;
	}
}